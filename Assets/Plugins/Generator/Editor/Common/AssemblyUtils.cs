using System;
using System.Collections.Generic;

namespace Generator.Common
{
	public class AssemblyUtils
	{
		public static List<Type> FindTypes(Func<Type, bool> filter)
		{
			var list = new List<Type>();
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (var assembly in assemblies)
			{
				var types = assembly.GetTypes();
				foreach (var type in types)
				{
					if (filter(type))
						list.Add(type);
				}
			}

			return list;
		}
	}
}