using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Generator.Common
{
    public abstract class AGeneratorWindowEditor<TGenerator> : EditorWindow
        where TGenerator : class, IGeneratorProvider, new()
    {
        private const string DEFAULT_PATH = "Generated";
        private const string GENERATION_PATH_KEY = "GenerationPath";

        private static string _generationPathKey;
        private static string _defaultPath;
        private static string _generationPath;

        private static TGenerator _generator;

        private static TGenerator Generator
        {
            get
            {
                if (_generator == null)
                {
                    _generator = new TGenerator();
                    _generationPathKey = _generator.Name + "." + GENERATION_PATH_KEY;
                    _defaultPath = Path.Combine(DEFAULT_PATH, _generator.Name);
                }

                return _generator;
            }
        }

        protected static void OpenWindow<TWindow>()
            where TWindow : AGeneratorWindowEditor<TGenerator>
        {
            var window = GetWindowWithRect<TWindow>(new Rect(0, 0, 300, 50), false, "Proxifier");
            window.Show();
        }

        protected static void Generate()
        {
            var generatorProvider = Generator;
            _generationPath = EditorPrefs.GetString(_generationPathKey, _defaultPath);
            if (string.IsNullOrEmpty(_generationPath))
            {
                UnityEngine.Debug.LogError($"[{typeof(TGenerator).Name}] Generation path can't be empty!");
                return;
            }

            var directory = new DirectoryInfo(Path.Combine(Application.dataPath, _generationPath));
            var files = generatorProvider.Generate(directory);
            Save(files);
            AssetDatabase.Refresh();
        }

        private void OnEnable()
        {
            var generatorProvider = Generator;
            _generationPath = EditorPrefs.GetString(_generationPathKey, _defaultPath);
        }

        private void OnGUI()
        {
            DrawGenerationPath();
            DrawGenerateButton();
        }

        private void DrawGenerationPath()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("GenerationPath");
            _generationPath = GUILayout.TextField(_generationPath);
            GUILayout.EndHorizontal();
        }

        private void DrawGenerateButton()
        {
            if (!GUILayout.Button("Generate"))
                return;
            Generate();
        }

        private void OnDisable()
        {
            EditorPrefs.SetString(_generationPathKey, _generationPath);
        }

        private static void Save(List<GeneratedFile> files)
        {
            foreach (var file in files)
            {
                var fileInfo = file.FileInfo;
                if (!fileInfo.Directory.Exists)
                    fileInfo.Directory.Create();
                if (fileInfo.Exists)
                    fileInfo.Delete();
                File.WriteAllText(fileInfo.FullName, file.FileData);
            }
        }
    }
}