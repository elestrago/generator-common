using System.Collections.Generic;
using System.IO;

namespace Generator.Common
{
	public interface IGeneratorProvider
	{
		string Name { get; }

		List<GeneratedFile> Generate(DirectoryInfo directoryInfo);
	}
}